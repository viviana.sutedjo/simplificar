# SimplificAR
People with intellectual disabilities (ID), low knowledge of a language or people with dementia often have difficulties understanding complicated texts. In Germany, public authorities are obligated to provide more information in “Leichter Sprache” (simplified language), a language pattern that is easier to understand for people with ID, and therefore helps with the inclusion of people with difficulties understanding texts.

These texts are created manually and only exist sparsely in German official websites. We want to use Text Simplification to create an AR App that translates any text (newspapers, books, manuals…) into simplified language when taking a picture of it.
