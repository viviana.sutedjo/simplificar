#  SimplificAR Frontend App
The Frontend App is written in Flutter. It mainly shows the camera feed and when the user takes a photo, the image is analyzed for text. The found text is then sent to the backend using REST API, and the app awaits the response. When the simplified text returns, it is shown in the image as a replacement of the original text.
